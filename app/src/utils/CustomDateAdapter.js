import DateIOAdapter from "@mui/lab/AdapterMoment";

export default function CustomDateAdapter(options) {
  const adapter = new DateIOAdapter(options);

  const constructUpperObject = (text) => ({ toUpperCase: () => text });
  const constructDayObject = (day) => ({
    charAt: () => constructUpperObject(day),
  });

  return {
    ...adapter,

    getWeekdays() {
      const customWeekdays = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];

      return customWeekdays.map((day) => constructDayObject(day));
    },
  };
}
