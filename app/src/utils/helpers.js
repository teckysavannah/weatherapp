import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCloud,
  faCloudSunRain,
  faCloudSun,
  faCloudShowersHeavy,
  faCloudShowersWater,
  faSun,
} from "@fortawesome/free-solid-svg-icons";
export function ParseDate(date) {
  return new Intl.DateTimeFormat("en-GB", {
    year: "numeric",
    month: "short",
    day: "numeric",
  }).format(date);
}

export function ParseCSVArrayToObject(csvArray) {
  const arr = [];
  csvArray.map((item) => {
    const obj = {
      Date: item[0],
      Rise: item[1],
      Tran: item[2],
      Set: item[3],
    };
    arr.push(obj);
  });
  return arr;
}
export const coordinate = [
  {
    location: "King's Park",
    coordinate: [22.3111031, 114.1712442],
  },
  { location: "Hong Kong Observatory", coordinate: [22.3021669, 114.1698424] },
  { location: "Wong Chuk Hang", coordinate: [22.2391622, 114.1612247] },
  { location: "Ta Kwu Ling", coordinate: [22.539334, 114.161842] },
  { location: "Lau Fau Shan", coordinate: [22.4668031, 113.9822968] },
  { location: "Tai Po", coordinate: [22.4767434, 114.1500721] },
  { location: "Sha Tin", coordinate: [22.3887767, 114.1820205] },
  { location: "Tuen Mun", coordinate: [22.3954805, 113.9314977] },
  { location: "Tseung Kwan O", coordinate: [22.3076328, 114.2559792] },
  { location: "Sai Kung", coordinate: [22.408131, 114.2466366] },
  { location: "Cheung Chau", coordinate: [22.2095525, 114.0204475] },
  { location: "Chek Lap Kok", coordinate: [22.3071387, 113.9017145] },
  { location: "Tsing Yi", coordinate: [22.3444757, 114.080779] },
  { location: "Shek Kong", coordinate: [22.4296185, 114.0769012] },
  { location: "Tsuen Wan Ho Koon", coordinate: [22.3837309, 114.105718] },
  {
    location: "Tsuen Wan Shing Mun Valley",
    coordinate: [22.3393736, 114.1145122],
  },
  { location: "Hong Kong Park", coordinate: [22.2771686, 114.1586582] },
  {
    location: "Shau Kei Wan",
    coordinate: [22.2787301, 114.2242379],
  },
  {
    location: "Kowloon City",
    coordinate: [22.3219813, 114.1717764],
  },
  {
    location: "Happy Valley",
    coordinate: [22.2698928, 114.1796292],
  },
  {
    location: "Wong Tai Sin",
    coordinate: [22.3524937, 114.1698684],
  },
  {
    location: "Stanley",
    coordinate: [22.2213362, 114.2046818],
  },
  {
    location: "Kwun Tong",
    coordinate: [22.3120123, 114.2193281],
  },
  {
    location: "Sham Shui Po",
    coordinate: [22.3290706, 114.1522452],
  },
  {
    location: "Kai Tak Runway Park",
    coordinate: [22.3048845, 114.2140157],
  },
  {
    location: "Yuen Long Park",
    coordinate: [22.4417941, 114.0165903],
  },
  {
    location: "Tai Mei Tuk",
    coordinate: [22.4734478, 114.2312844],
  },
];

export function WeatherIcon(weatherMSG) {
  const msg = weatherMSG.toLowerCase();
  function Has(str) {
    return msg.includes(str);
  }
  if ((Has("fine") || Has("sunny") || Has("hot")) && Has("cloudy")) {
    return <FontAwesomeIcon icon={faCloudSun} />;
  }
  if (
    ((Has("sunny") || Has("fine")) && Has("cloudy") && Has("showers")) ||
    (Has("few showers") && Has("cloudy"))
  ) {
    return <FontAwesomeIcon icon={faCloudSunRain} />;
  }
  if (Has("heavy")) {
    return <FontAwesomeIcon icon={faCloudShowersHeavy} />;
  }
  if (Has("showers")) {
    return <FontAwesomeIcon icon={faCloudShowersWater} />;
  }
  if (Has("cloudy")) {
    return <FontAwesomeIcon icon={faCloud} />;
  }
  return <FontAwesomeIcon icon={faSun} />;
}

export function GetYear(dateStr) {
  return dateStr.slice(0, 4);
}
export function GetMonth(dateStr) {
  return dateStr.slice(5, 7) - 1;
}
export function GetDay(dateStr) {
  return dateStr.slice(-2);
}
