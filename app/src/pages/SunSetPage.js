import React, { useEffect, useState } from "react";
import axios from "axios";
import { usePapaParse } from "react-papaparse";
import CustomDateAdapter from "../utils/CustomDateAdapter";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import CustomDateRangePicker from "../components/CustomizedDateRangePicker";
import { Box, Typography } from "@mui/material";
import {
  ParseCSVArrayToObject,
  ParseDate,
  GetYear,
  GetMonth,
  GetDay,
} from "../utils/helpers";
import SunRiseSetCard from "../components/SunRiseSetCard";
import DateChart from "../components/DateChart";

export default function SunSetPage() {
  const [riseAndSetData, setRiseAndSetData] = useState();
  const [timePeriod, setTimePeriod] = useState([]);
  const [graphData, setGraphData] = useState({});

  const { readString } = usePapaParse();
  const getDatePickerValue = (newValue) => {
    setTimePeriod([new Date(newValue[0]), new Date(newValue[1])]);
    if (riseAndSetData) {
      MapSunData(riseAndSetData);
    }
  };
  const style = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    "& .updateTime": {
      marginBottom: "20px",
    },
    "& .dateRangePicker-container": {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      paddingTop: "20px",
    },
  };
  function MapSunData(sunData) {
    let rise = [];
    let set = [];
    sunData
      .filter(
        (item) =>
          new Date(item.Date) >= new Date(ParseDate(timePeriod[0])) &&
          new Date(item.Date) <=
            new Date(ParseDate(timePeriod[1])).setDate(
              new Date(ParseDate(timePeriod[1])).getDate() + 1
            )
      )
      .map((item) => {
        rise.push({
          x: new Date(
            GetYear(item.Date),
            GetMonth(item.Date),
            GetDay(item.Date)
          ),
          y: new Date(
            1970,
            0,
            3,
            item.Rise.slice(0, 2),
            item.Rise.slice(-2)
          ).getTime(),
        });
        set.push({
          x: new Date(
            GetYear(item.Date),
            GetMonth(item.Date),
            GetDay(item.Date)
          ),
          y: new Date(
            1970,
            0,
            3,
            item.Set.slice(0, 2),
            item.Set.slice(-2)
          ).getTime(),
        });
      });

    const data = { rise, set };
    console.log(data);
    setGraphData(data);
  }
  async function ReadCSV(csvString) {
    await readString(csvString, {
      worker: true,
      complete: (results) => {
        results.data.pop();
        results.data.shift();
        const CSVobj = ParseCSVArrayToObject(results.data);
        setRiseAndSetData(CSVobj);
      },
    });
  }
  async function fetchSunData() {
    try {
      const response = (await axios.get("http://localhost:8080/api/sunrise"))
        .data;
      ReadCSV(response);
    } catch (error) {
      console.log(error);
    }
  }
  useEffect(() => {
    fetchSunData();
  }, []);
  return (
    <>
      <LocalizationProvider dateAdapter={CustomDateAdapter}>
        <Box sx={style}>
          <Box className="card">
            <Box className="content" sx={{ overflow: "hidden" }}>
              <Typography variant="h5" component="h2">
                Sunrise / Sunset Time
              </Typography>
              <Box className="dateRangePicker-container">
                <CustomDateRangePicker
                  handleDatePickerValue={(newValue) =>
                    getDatePickerValue(newValue)
                  }
                />
              </Box>
            </Box>
          </Box>
          <Box className="card">
            <Box className="content" sx={{ "& a": { display: "none" } }}>
              <DateChart data={graphData} />
            </Box>
          </Box>
          <Box className="card">
            <Box className="content" sx={{ "& a": { display: "none" } }}>
              {/* <StockChart /> */}
              <p>Graph data</p>
              {JSON.stringify(graphData)}
              <p>Time period selected</p>
              {JSON.stringify(timePeriod)}
            </Box>
          </Box>
          <Box className="card">
            <Box className="content card-body">
              {riseAndSetData
                ? riseAndSetData
                    .filter(
                      (item) =>
                        new Date(item.Date) >=
                          new Date(ParseDate(timePeriod[0])) &&
                        new Date(item.Date) <=
                          new Date(ParseDate(timePeriod[1])).setDate(
                            new Date(ParseDate(timePeriod[1])).getDate() + 1
                          )
                    )
                    .map((item) => (
                      <SunRiseSetCard
                        key={item.Date}
                        date={item.Date}
                        sunRise={item.Rise}
                        sunSet={item.Set}
                      />
                    ))
                : null}
            </Box>
          </Box>
        </Box>
      </LocalizationProvider>
    </>
  );
}
