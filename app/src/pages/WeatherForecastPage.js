import React, { useEffect, useState } from "react";
import axios from "axios";
import DataGrid from "../components/DataGrid";
import { Box, Typography } from "@mui/material";
import MultiSeriesChart from "../components/MultiSeriesChart";

export default function WeatherForecastPage() {
  const [weather, setWeather] = useState([]);
  const [graphData, setGraphData] = useState();

  const style = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100vw",
    flexDirection: "column",
    "& .topic": {
      marginBottom: "20px",
      height: "auto",
    },
  };

  function MapTempGraphData(tempData) {
    let maxTemp = [];
    let minTemp = [];
    tempData.map((item) => {
      maxTemp.push({
        y: item.forecastMaxtemp.value,
        label: item.week.slice(0, 3),
      });
      minTemp.push({
        y: item.forecastMintemp.value,
        label: item.week.slice(0, 3),
      });
    });
    const tempGraphData = {
      max: { name: "Max Temp", data: maxTemp },
      min: { name: "Min Temp", data: minTemp },
    };
    setGraphData(tempGraphData);
  }

  const url =
    "https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=fnd&lang=en";
  async function fetchData() {
    const response = (await axios.get(url)).data;
    setWeather(response);
    MapTempGraphData(response.weatherForecast);
  }
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <Box sx={style}>
      <Box className="card">
        <Box className={"content"}>
          <Typography variant="h5" component="h1">
            9 Days Forecast
          </Typography>
          {weather ? (
            <Typography>
              As of {weather.updateTime?.slice(0, 10)},{" "}
              {weather.updateTime?.slice(11, 16)} HKT
            </Typography>
          ) : null}
        </Box>
      </Box>
      <Box className="card">
        <Box className="content" sx={{ "& a": { display: "none" } }}>
          <MultiSeriesChart
            title="Weather Forecast"
            axisY="Temperature"
            tempData={graphData}
          />
        </Box>
      </Box>
      <Box className="card">
        <Box className="content card-body">
          <DataGrid weather={weather.weatherForecast} />
        </Box>
      </Box>
    </Box>
  );
}
