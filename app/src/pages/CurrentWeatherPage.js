import React, { useEffect, useState } from "react";
import GoogleMap from "../components/GoogleMap";
import axios from "axios";
import { Box, Typography } from "@mui/material";
export default function CurrentWeatherPage() {
  const [currentWeather, setCurrentWeather] = useState();
  const style = {
    width: "100vw",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    "& .card-body > div": {
      width: "100%",
      height: "100%",
    },
  };

  const url =
    " https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=rhrread&lang=en";
  async function fetchCurrentData() {
    const response = (await axios.get(url)).data;
    console.log(response);
    setCurrentWeather(response);
  }

  useEffect(() => {
    fetchCurrentData();
  }, []);
  return (
    <Box sx={style}>
      <Box className="card">
        <Box className="content">
          <Typography variant="h5" component="h2">
            Current Regional Weather
          </Typography>
          <Typography>
            As of {currentWeather?.updateTime.slice(0, 10)},{" "}
            {currentWeather?.updateTime.slice(11, 16)} HKT
          </Typography>
        </Box>
      </Box>
      <Box className="card">
        <Box className="content card-body">
          {currentWeather ? <GoogleMap weather={currentWeather} /> : null}
        </Box>
      </Box>
    </Box>
  );
}
