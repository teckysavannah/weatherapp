import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import WeatherForecastPage from "./pages/WeatherForecastPage";
import CurrentWeatherPage from "./pages/CurrentWeatherPage";
import SunSetPage from "./pages/SunSetPage";
import reportWebVitals from "./reportWebVitals";
import { LicenseInfo } from "@mui/x-license-pro";

LicenseInfo.setLicenseKey(process.env.REACT_APP_MUI_LICENSE_KEY);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<WeatherForecastPage />} />
          <Route path="weatherforecast" element={<WeatherForecastPage />} />
          <Route path="sunset" element={<SunSetPage />} />
          <Route path="currentweather" element={<CurrentWeatherPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
