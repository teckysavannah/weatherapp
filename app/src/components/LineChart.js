import React from "react";
import { Box } from "@mui/material";
import CanvasJSChart from "../assets/canvasjs.react";
import CanvasJSReact from "../assets/canvasjs.stock.react";
const CanvasJS = CanvasJSReact.CanvasJS;
export default function LineChart({ title, axisX, axisY, sunData }) {
  // const options = {
  //   theme: "light2",
  //   animationEnabled: true,
  //   exportEnabled: true,
  //   title:{
  //     text: title
  //   },
  //   axisY: {
  //     title: axisY,
  //     suffix: " °F"
  //   },
  //   axisX: {
  //     valueFormatString: "MMM YYYY"
  //   },
  //   data: [
  //   {
  //     type: "rangeArea",
  //     xValueFormatString: "MMM YYYY",
  //     yValueFormatString: "#00:00 AM",
  //     toolTipContent: " <span style=\"color:#6D78AD\">{x}</span><br><b>Min:</b> {y[0]}<br><b>Max:</b> {y[1]}",
  //     dataPoints: sunData?.rise.dataPoints
  //   }]
  // }
  const options = {
    title: {
      text: "How long an event occurred for on a given day",
    },
    axisX: {
      interval: 1,
      intervalType: "day",
      valueFormatString: "DD MMM",
    },
    axisY: {
      minimum: new Date(1970, 0, 3, 0, 0, 0).getTime(),
      interval: 1 * 60 * 60 * 1000,
      labelFormatter: function (e) {
        return CanvasJS.formatDate(e.value, "hh:mm TT");
      },
      //suffix : " hour"
    },

    toolTip: {
      contentFormatter: function (e) {
        return (
          "Duration<br>" +
          CanvasJS.formatDate(e.entries[0].dataPoint.y, "hh:mm TT")
        );
      },
    },

    data: [
      {
        type: "column",
        dataPoints: [
          { x: new Date(2010, 0, 3), y: new Date(1970, 0, 3, 1, 0).getTime() },
          { x: new Date(2010, 0, 4), y: new Date(1970, 0, 3, 2, 0).getTime() },
          { x: new Date(2010, 0, 5), y: new Date(1970, 0, 3, 3, 0).getTime() },
          { x: new Date(2010, 0, 6), y: new Date(1970, 0, 3, 4, 0).getTime() },
        ],
      },
    ],
  };
  return (
    <Box
      sx={{ padding: "50px", backgroundColor: "white", borderRadius: "10px" }}
    >
      <CanvasJSChart options={options} />
    </Box>
  );
}

// const options = {
//   theme: "light2",
//   title: {
//     text: "",
//   },

//   axisY: {
//     intervalType: "hour",
//     valueFormatString: "HH:mm",
//   },
//   toolTip: {
//     shared: true,
//   },

//   data: [
//     {
//       type: "area",
//       name: sunData?.rise.name,
//       // showInLegend: true,
//       xValueFormatString: "DD MMM",
//       dataPoints: sunData?.rise.dataPoints,
//     },
//     {
//       type: "area",
//       name: sunData?.set.name,
//       // showInLegend: true,
//       xValueFormatString: "DD MMM",
//       dataPoints: sunData?.set.dataPoints,
//     },
//   ],
// };
