import React from "react";
import { Link } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import MenuItem from "@mui/material/MenuItem";

export default function NavBar({ children }) {
  const pages = [
    { text: "Weather Forecast", link: "/weatherforecast" },
    { text: "Current Weather", link: "/currentweather" },
    { text: "Sunrise/Sunset", link: "/sunset" },
  ];

  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <nav>
      <AppBar
        position="static"
        sx={{
          backgroundImage: "linear-gradient(120deg, #b7b2e4 0%, #da96af 100%)",
          marginBottom: "5vh",
          "& .navFont": {
            color: "#fff",
            paddingRight: "20px",
          },
        }}
      >
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: "block", md: "none" },
                  "& .MuiList-root": { backgroundColor: "#333333" },
                }}
              >
                {pages.map((page) => (
                  <MenuItem key={page.text} onClick={handleCloseNavMenu}>
                    <Link to={page.link}>
                      <Typography textAlign="center" sx={{ color: "white" }}>
                        {page.text}
                      </Typography>
                    </Link>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              {pages.map((page) => (
                <Link
                  className="navFont"
                  to={page.link}
                  key={page.text}
                  sx={{ my: 2, color: "white", display: "block" }}
                >
                  {page.text}
                </Link>
              ))}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </nav>
  );
}
