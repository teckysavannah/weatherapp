import React, { useEffect, useState } from "react";
import { useGridApiRef, DataGridPro } from "@mui/x-data-grid-pro";
import { Box } from "@mui/material";
import fontawesome from "@fortawesome/fontawesome";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faWind,
  faDroplet,
  faTemperatureThreeQuarters,
} from "@fortawesome/free-solid-svg-icons";
import { WeatherIcon } from "../utils/helpers";

export default function DataGrid({ weather }) {
  const [color, setColor] = useState("primary");
  const apiRef = useGridApiRef();
  const dataGridStyle = {
    width: "100%",
    "& .MuiDataGrid-columnHeaders": {
      backgroundColor: "#424242",
      color: "#FFF",
      textAlign: "start",
    },

    "& .MuiDataGrid-columnHeaderTitle": {
      marginLeft: "6px",
      fontSize: "16px",
      fontWeight: "500",
    },
    "& .MuiDataGrid-columnHeader:focus, & .MuiDataGrid-columnHeader:focus-within,& .MuiDataGrid-cell:focus,& .MuiDataGrid-cell:focus-within":
      {
        outline: "none",
      },

    /* To hide the last grid column separator ------------*/
    "& .MuiDataGrid-columnHeader:last-child .MuiDataGrid-columnSeparator": {
      visibility: "hidden",
    },
    "& .MuiDataGrid-columnHeader button": {
      color: "#FFF",
    },

    /* To hide dataGrid sortIcon ------------*/
    "& .MuiDataGrid-sortIcon": {
      opacity: "100",
    },
    "& svg": { height: "2em", marginRight: "5px" },
  };
  const column = [
    {
      field: "week",
      headerName: "Date",
      width: 150,
      flex: 0.7,
      sortable: false,
      renderCell: (params) => (
        <Box>
          {` ${params.row.week.slice(0, 3)} / 
          ${params.row.forecastDate.slice(-2)}`}
        </Box>
      ),
    },
    {
      field: "temp",
      headerName: "Temperature",
      width: 150,
      flex: 0.8,
      sortable: false,

      renderCell: (params) => (
        <Box sx={{ display: "flex" }}>
          <FontAwesomeIcon icon={faTemperatureThreeQuarters} />
          {`${params.row.forecastMaxtemp.value}°${params.row.forecastMaxtemp.unit} / 
            ${params.row.forecastMintemp.value}°${params.row.forecastMintemp.unit}`}
        </Box>
      ),
    },
    {
      field: "Forecast Weather",
      width: 150,
      flex: 1.5,
      sortable: false,

      renderCell: (params) => (
        <Box sx={{ display: "flex" }}>
          {WeatherIcon(params.row.forecastWeather)}
          {params.row.forecastWeather}
        </Box>
      ),
    },
    {
      field: "Humidity",
      width: 150,
      flex: 0.8,
      sortable: false,

      renderCell: (params) => (
        <Box sx={{ display: "flex" }}>
          <FontAwesomeIcon icon={faDroplet} />
          {`${params.row.forecastMinrh.value}% - ${params.row.forecastMaxrh.value}%`}
        </Box>
      ),
    },
    {
      field: "Forecast Wind",
      width: 150,
      flex: 2.5,
      sortable: false,

      renderCell: (params) => (
        <Box sx={{ display: "flex" }}>
          <FontAwesomeIcon icon={faWind} />
          {`${params.row.forecastWind}`}
        </Box>
      ),
    },
  ];

  return (
    <div style={{ height: "100%", width: "100%" }}>
      <DataGridPro
        sx={dataGridStyle}
        className="glassy"
        getRowId={(row) => row.forecastDate}
        apiRef={apiRef}
        columns={column}
        rows={weather ? weather : []}
        rowHeight={70}
        disableColumnMenu
        hideFooter
        componentsProps={{
          columnMenu: { color },
        }}
      />
    </div>
  );
}
