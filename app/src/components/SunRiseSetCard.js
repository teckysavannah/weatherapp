import React from "react";
import { Box } from "@mui/material";
import { ParseDate } from "../utils/helpers";

export default function SunRiseSetCard({ date, sunRise, sunSet }) {
  const style = {
    width: " 40vw",
    margin: " 5px auto",
    borderRadius: " 15px",
    padding: " 15px 30px",
    marginBottom: "15px",
    "& .date": {
      textShadow: "1px 1px 7px rgba(0, 0, 0, 1)",
      fontSize: " 16px",
      fontWeight: "700",
      color: "#FCFCFC",
      flex: "3",
    },
    "& .sunContent": {
      display: " flex",
      height: "5vh",
    },
    "& .sunContent div": {
      textShadow: "1px 1px 3px rgba(0, 0, 0, 1)",
    },
    "& .sunrise, & .sunset": {
      fontSize: " 16px",
      fontWeight: "700",
      display: " flex",
      alignItems: "center",
      flex: "4.5",
    },
    "& img": {
      height: "70%",
    },
    "& .time": {
      fontSize: " 23px",
      flex: "8",
    },
    "& .icon": {
      height: "100%",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      flex: "4",
    },
  };
  return (
    <Box sx={style} key={date} className="glassy">
      <div className="sunContent">
        <div className="date">{ParseDate(new Date(date))}</div>
        <div className="sunrise">
          <div className="icon">
            <img src="/sunrise.svg" alt="sunriseIMG" />
            Sunrise
          </div>
          <div className="time">{sunRise}</div>
        </div>
        <div className="sunset">
          <div className="icon">
            <img src="/sunset.svg" alt="sunsetIMG" />
            Sunset
          </div>
          <div className="time">{sunSet}</div>
        </div>
      </div>
    </Box>
  );
}
