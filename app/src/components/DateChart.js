import React, { Component } from "react";
import CanvasJSReact from "../assets/canvasjs.stock.react";
const CanvasJS = CanvasJSReact.CanvasJS;
const CanvasJSStockChart = CanvasJSReact.CanvasJSStockChart;

export default function DateChart({ data }) {
  const options = {
    title: {
      text: "------",
    },
    animationEnabled: true,
    exportEnabled: true,
    charts: [
      {
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
          },
          interval: 1,
          intervalType: "month",
          valueFormatString: "DD MMM",
        },
        axisY: {
          crosshair: {
            enabled: true,
          },
          minimum: new Date(1970, 0, 3, 0, 0, 0).getTime(),
          interval: 1 * 60 * 60 * 1000,
          labelFormatter: function (e) {
            return CanvasJS.formatDate(e.value, "hh:mm TT");
          },
        },
        data: [
          {
            type: "spline",
            dataPoints: [
              {
                x: new Date(2010, 0, 3),
                y: new Date(1970, 0, 3, 1, 0).getTime(),
              },
              {
                x: new Date(2010, 0, 4),
                y: new Date(1970, 0, 3, 2, 0).getTime(),
              },
              {
                x: new Date(2010, 0, 5),
                y: new Date(1970, 0, 3, 3, 0).getTime(),
              },
              {
                x: new Date(2010, 0, 6),
                y: new Date(1970, 0, 3, 4, 0).getTime(),
              },
            ],
            // data.rise,
          },
          {
            type: "spline",

            dataPoints: [
              {
                x: new Date(2010, 0, 3),
                y: new Date(1970, 0, 3, 3, 0).getTime(),
              },
              {
                x: new Date(2010, 0, 4),
                y: new Date(1970, 0, 3, 5, 0).getTime(),
              },
              {
                x: new Date(2010, 0, 5),
                y: new Date(1970, 0, 3, 10, 0).getTime(),
              },
              {
                x: new Date(2010, 0, 6),
                y: new Date(1970, 0, 3, 14, 0).getTime(),
              },
            ],
            // data.set,
          },
        ],
      },
    ],
    toolTip: {
      contentFormatter: function (e) {
        return (
          "Duration<br>" +
          CanvasJS.formatDate(e.entries[0].dataPoint.y, "hh:mm TT")
        );
      },
    },
    rangeSelector: {
      inputFields: {
        startValue: 5000,
        endValue: 6000,
        valueFormatString: "###0",
      },
    },
  };
  const containerProps = {
    width: "100%",
    height: "450px",
    margin: "auto",
  };

  return (
    <CanvasJSStockChart containerProps={containerProps} options={options} />
  );
}
