import React from "react";
import CanvasJSChart from "../assets/canvasjs.react";
import { Box } from "@mui/material";
import { borderRadius } from "@mui/system";

export default function MultiSeriesChart({ title, axisY, tempData }) {
  const options = {
    animationEnabled: true,
    title: {
      text: title,
    },
    axisY: {
      title: axisY,
      viewportMaximum: 45,
    },
    toolTip: {
      shared: true,
    },
    data: [
      {
        type: "spline",
        name: tempData?.max.name,
        showInLegend: true,
        dataPoints: tempData?.max.data,
      },
      {
        type: "spline",
        name: tempData?.min.name,
        showInLegend: true,
        dataPoints: tempData?.min.data,
      },
    ],
  };
  return (
    <Box
      sx={{ padding: "50px", backgroundColor: "white", borderRadius: "10px" }}
    >
      <CanvasJSChart options={options} />
    </Box>
  );
}
