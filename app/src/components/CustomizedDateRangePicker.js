import React, { useEffect, useState } from "react";
import { TextField, Box, InputAdornment } from "@mui/material/";
import { DateRangePicker } from "@mui/x-date-pickers-pro/DateRangePicker";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import DateRangeOutlinedIcon from "@mui/icons-material/DateRangeOutlined";

export default function CustomizedDateRangePicker({
  handleDatePickerValue,
  dateValues,
}) {
  const [value, setValue] = useState([new Date(), new Date()]);
  const handleOnDateChange = (newValue) => {
    setValue(newValue);
    handleDatePickerValue(newValue);
  };
  const datePickerStyle = {
    display: "flex",
    alignItems: "center",
    height: "40px !important",
    width: "349px",
    boxSizing: "border-box",
    "& .MuiOutlinedInput-notchedOutline": { border: "none !important" },
    "& .MuiOutlinedInput-input": {
      textAlign: "center",
      color: "white",
      fontFamily:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
      textShadow: "1px 1px 3px rgba(0, 0, 0, 1)",
      fontWeight: "700",
    },
  };
  const datePickerDayStyle = {
    "& .MuiPickersDay-root.Mui-selected": {
      backgroundColor: "#de94a9!important",
    },
    "& .MuiDateRangePickerDay-rangeIntervalDayHighlight": {
      backgroundColor: "#f0d0e855 !important",
    },
  };
  useEffect(() => {
    if (dateValues) {
      setValue(dateValues);
    }
    handleDatePickerValue(value);
  }, [dateValues]);
  return (
    <>
      <Box sx={datePickerStyle} className="glassy">
        <DateRangePicker
          sx={datePickerDayStyle}
          startText="Start Date"
          endText="End Date"
          value={value}
          onChange={(newValue) => {
            handleOnDateChange(newValue);
          }}
          componentsProps={{
            switchViewButton: { OpenPickerIcon: ArrowDropDownIcon },
          }}
          renderInput={(startProps, endProps) => (
            <>
              <TextField
                {...startProps}
                inputProps={{
                  ...startProps.inputProps,
                  placeholder: "Start",
                  autoComplete: "off",
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <DateRangeOutlinedIcon />
                    </InputAdornment>
                  ),
                }}
              />
              <Box sx={{ mx: 2 }}> to </Box>
              <TextField
                {...endProps}
                inputProps={{
                  ...endProps.inputProps,
                  placeholder: "End",
                  autoComplete: "off",
                }}
              />
            </>
          )}
        />
      </Box>
    </>
  );
}
