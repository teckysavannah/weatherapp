import React, { Component } from "react";
import CanvasJSReact from "../assets/canvasjs.stock.react";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSStockChart = CanvasJSReact.CanvasJSStockChart;

export default function StockChart() {
  const options = {
    title: {
      text: "React StockChart with Numeric Axis",
    },
    animationEnabled: true,
    exportEnabled: true,
    charts: [
      {
        axisX: {
          crosshair: {
            enabled: true,
            snapToDataPoint: true,
          },
        },
        axisY: {
          crosshair: {
            enabled: true,
            //snapToDataPoint: true
          },
        },
        data: [
          {
            type: "spline",
            dataPoints: generateDataPoints(10000),
          },
        ],
      },
    ],
    rangeSelector: {
      inputFields: {
        startValue: 4000,
        endValue: 6000,
        valueFormatString: "###0",
      },
    },
  };
  function generateDataPoints(noOfDps) {
    var xVal = 1,
      yVal = 100;
    var dps = [];
    for (var i = 0; i < noOfDps; i++) {
      yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
      dps.push({ x: xVal, y: yVal });
      xVal++;
    }
    return dps;
  }
  const containerProps = {
    width: "100%",
    height: "450px",
    margin: "auto",
  };

  return (
    <CanvasJSStockChart
      containerProps={containerProps}
      options={options}
      /* onRef = {ref => this.chart = ref} */
    />
  );
}
