import React, { useEffect, useState } from "react";
import { Map, GoogleApiWrapper, Marker, InfoWindow } from "google-maps-react";
import { Box } from "@mui/material";
import { coordinate } from "../utils/helpers";

function GoogleMap({ google, weather }) {
  const [showingInfoWindow, setShowingInfoWindow] = useState(false);
  const [activeMarker, setActiveMarker] = useState({});
  const [currentWeather, setCurrentWeather] = useState();
  const mapStyles = {
    width: "100%",
    height: "100%",
  };
  const style = {
    width: "100%",
    height: "100%",
    borderRadius: "20px",
    overflow: "hidden",
    position: "relative",
    "& button:focus-visible,& div,& iframe": {
      border: "none !important",
      outline: " none !important",
    },
    "& button.gm-ui-hover-effect": {
      display: "none !important",
    },
    "& .info-window": {
      color: "#000",
    },
  };
  const onMouseover = (props, marker, e) => {
    if (!showingInfoWindow) {
      setShowingInfoWindow(true);
      setActiveMarker(marker);
    }
  };
  const onMouseout = () => {
    if (showingInfoWindow) {
      setShowingInfoWindow(false);
      setActiveMarker({});
    }
  };

  useEffect(() => {
    setCurrentWeather(weather);
  });
  return (
    <>
      <Box sx={style}>
        {currentWeather ? (
          <Map
            google={google}
            zoom={11}
            style={mapStyles}
            disableDefaultUI={true}
            initialCenter={{ lat: 22.380189, lng: 114.138079 }}
          >
            {coordinate.map((item) => (
              <Marker
                key={item.location}
                name={item.location}
                position={{ lat: item.coordinate[0], lng: item.coordinate[1] }}
                onMouseover={(props, marker) => onMouseover(props, marker)}
                onMouseout={() => onMouseout()}
              />
            ))}
            <InfoWindow
              style={mapStyles}
              marker={activeMarker}
              visible={showingInfoWindow}
            >
              <div className="info-window">
                <div>{activeMarker.name}</div>
                {currentWeather.temperature.data
                  .filter((item) => item.place === activeMarker.name)
                  .map((item) => (
                    <div key={item.place}>{`${item.value}°${item.unit}`}</div>
                  ))}
              </div>
            </InfoWindow>
          </Map>
        ) : (
          <p>Loading</p>
        )}
      </Box>
    </>
  );
}
export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GOOGLE_API_KEY,
})(GoogleMap);
// GOOGLE API KEY (Maps Javascript API enabled) is needed