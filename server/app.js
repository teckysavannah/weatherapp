import cors from "cors";
import express from "express";
import path from "path";
import dotenv from "dotenv";
import axios from "axios";

dotenv.config();

const app = express();
// URLENCODED => JS
app.use(express.urlencoded({ extended: true }));
app.use(cors());
// JSON string => JS
app.use(express.json());
app.use(express.static(path.resolve("public")));

try {
  app.get("/api/sunrise", async (req, res) => {
    console.log("get sunrise");
    const result = (
      await axios.get(
        "https://data.weather.gov.hk/weatherAPI/opendata/opendata.php?dataType=SRS&year=2022&rformat=csv"
      )
    ).data;
    res.json(result);
  });
} catch (error) {
  console.log(error);
}

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening to http://localhost:${PORT}`);
});
