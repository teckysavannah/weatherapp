# Getting Started

## This app allows users to check the Hong Kong Weather forecast.

### App features:
- 9 days weather forecast
- Regional current weather temperature
- Sunrise and Sunset time


#
# Available Scripts
In the project directory, you have to:

## cd `app` and run `yarn start` to start the app

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


## cd `server` and run `yarn start` to start the server

Server runs at http://localhost:8080
#

# Remarks
***
 ### GOOGLE API KEY (Maps Javascript API enabled) is needed in .env
***

### Current Weather Page:
- Moving mouse above the marker on the map to checkout the regional temperture.
### Sunrise/Sunset Page:
- Use date range picker to select specific range of date.
- by default, showing today's sunrise/sunset data.
- Sunrise/Sunset data only available in 2022.